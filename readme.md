# The M.E.B.N Stack. with MongoDB, Express, Backbone.js, & Node.js #

"Cookie Jar" is a sample CRUD application boilerplate.

The application allows you to browse through a list of cookies, add, update, and delete.
Application built with MongoDB, Express, Backbone.js, Node.js

# Features now include:
    Modular structure
    Automated compiling with GULP, COMPASS, SCSS
